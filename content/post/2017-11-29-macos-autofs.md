---

title: 'Automatically Mount Network Drives using Autofs on MacOS'
slug: macos-autofs
categories:
- Tech

---

*Update, 2020-04-08:* I now use [Arq with MinIO](/2020/04/08/synology-backup-guide/), which I find faster and more reliable.

I use [Arq](https://www.arqbackup.com/) to backup my Mac both to Dropbox and to my [Synology NAS](https://www.synology.com/en-us/products/series/plus), but I've had difficulty keeping the SMB network share mounted and available for Arq. The solution to this problem is Autofs ([Apple White Paper](https://loga.us/wp-content/uploads/2014/09/Autofs.pdf)).

Autofs is an automounter, a program for 'automatically mounting directories on an as-needed basis', that was first released in Solaris 2.0, and comes preinstalled on MacOS since 10.5. This means, once configured, Autofs will automatically mount network shares and reconnect them if my NAS is disconnected for whatever reason. Here's how I setup Autofs ([`jamesramsay/dotfiles cf11730`](https://github.com/jamesramsay/dotfiles/commit/cf11730f7fafb8b8d33585bc75290d85ae04b243)).

### Create a mount point

The mount point is the directory where Autofs will mount the network share. We will mount the share in `/mnt/Synology`.

From the Terminal, create the `/mnt` directory:

```
sudo mkdir /mnt
```

### Add a map to the master map

Each map in the master map consists of a mount point (`/mnt/Synology`) and a mount map file. We will create the mount map file (`auto_synology`) in the next step.

Add the map to the master map:

```
echo "/mnt/Synology		auto_synology" | \
  sudo tee -a /etc/auto_master
```

Refer to [`auto_master` documentation](https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man5/auto_master.5.html) for more details.

### Create the mount map file

The mount map file must be located in the `/etc` directory. To open the new mount map file for editing, execute:

```
sudo $EDITOR /etc/auto_synology
```

The mount map file is a list of trigger folders and network shares that will be mounted automatically. Add a line for each share to be mounted automatically:

```
Arq    -fstype=smbfs,rw,noowners ://user:password@1.2.3.4/Arq
```

The mount map file above has only one mount. Autofs will mount our network share `://user:password@1.2.3.4/Arq` at `/mnt/Synology/Arq`. The arguments between the trigger location and network share specify the SMB protocol (`smbfs`), to mount the share for read and write access (`rw`) and to mount the share with permissions inherited from the mount point (`noowners`).

Refer to the [`mount` documentation](https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man8/mount.8.html) for all the supported options.

### Update automount

To finish setting up Autofs we need to flush the cache, which will cause Autofs to reload the `/etc/auto_master` file. Using [automount](https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man8/automount.8.html), execute:

```
sudo automount -vc
```

The cache will be flushed and the share we configured should be mounted. Autofs will now automatically mount the network share at boot and whenever it is needed by Arq.


