---

title: Automatic screenshot compression
slug: automatic-screenshot-compression
date: 2020-03-29T09:40:56Z
categories:
- Tech

---

(Summarized from my post for GitLab,
[One simple trick to make your screenshots 80% smaller](https://about.gitlab.com/blog/2020/01/30/simple-trick-for-smaller-screenshots/))

When you capture a screenshot on your Mac, it will be saved in the PNG-32 format, with support for 16 million distinct colors and transparency. This means that the screenshot will perfectly capture every pixel on your screen, but having four 8-bit channels for red, green, blue and alpha (transparency) for every pixel makes the file very large. If you're interested, you can verify this yourself using [pngcheck](http://www.libpng.org/pub/png/apps/pngcheck.html).

In practice, the subjects of my screenshots are buttons and forms, not photographs. Since we don't need 16 million colors, we can take advantage of the PNG-8 format with it's more compact 256 color palette.

### Lossy Compression: Color Quantization

The first step is to reduce the color palette of the screenshot. This is a type of lossy compression called [color quantization](https://en.wikipedia.org/wiki/Color_quantization), which will reduce the number of distinct colors in the image. The [pngquant](https://pngquant.org/) command line utility is the perfect tool for this job, and if you've used the popular [ImageAlpha tool](https://pngmini.com/), you've already used the pngquant library.

```bash
# Install pngquant using Homebrew
brew install pngquant

# Quantize 32-bit RGBA PNG to 8-bit (or smaller) RGBA-palette
# pngquant [number of colors] [options] input.png
#   --skip-if-larger  only save converted file if they're smaller than original
#   --strip           remove optional metadata
#   --ext=.png        set output filename to be same as input filename
#   --force           overwrite existing output files
pngquant 256 --skip-if-larger --strip --ext=.png --force example.png
```

I've observed for most screenshots you can comfortably reduce the color palette to as few as 64 colors before it becomes noticeable. If you frequently take screenshots of gradients or more complex images, you may want to stick with 256 colors to avoid noticeable artifacts.

### Lossless Compression: DEFLATE

The PNG file format uses [DEFLATE](https://en.wikipedia.org/wiki/DEFLATE) compression internally for an added layer of lossless compression, but most PNG libraries do not implement aggressive lossless compression. This provides another opportunity to reduce the file size further.

In 2013, Google released [zopfli](https://github.com/google/zopfli), which claimed to improve compression by 3-8% compared to `zlib`. The trade off for this improvement: waiting an extra 1-2 seconds. (There is no decompression penalty when viewing the compressed image).

```bash
# Install zopfli using Homebrew, which includes zopflipng
brew install zopfli

# Optimize PNG compression
# zopflipng [options] input.png output.png
#   -y  do not ask about overwriting files
zopflipng -y example.png example.png
```

Relative to the massive savings from color quantization, improving lossless compression provides a much smaller reduction, but in the context of pages with many images these marginal gains do add up to worthwhile savings.

### Automation

The trick is to make this happen automatically every time I capture a screenshot using [Automator](https://support.apple.com/en-au/guide/automator/welcome/mac). This allows you to run commands based on file events, like every time a new screenshot is added to a directory.

I create a dedicated Screenshots directory, so that they don't clutter your desktop.

```bash
# Create a Screenshots directory in the current users Home directory
mkdir -p "$HOME/Screenshots"

# Configure macOS to capture screenshots to this location
# If you want to revert this change, and save screenshots to your desktop,
# instead use "$HOME/Desktop"
defaults write com.apple.screencapture location "$HOME/Screenshots"
```

Using Automator, create a new **Folder Action** that receives notifications from the Screenshots folder. Add a **Run Shell Script** block, and make sure to **Pass input as arguments**. Combining the commands above, and this time using `$@` syntax to handle multiple arguments, and absolute paths for pngquant and zopflipng, the final script is:

```bash
for f in "$@"
do
  # Quantize to PNG-8 with 64 colors (lossy, fast)
  # Majority of file size savings come from this
  /usr/local/bin/pngquant 64 --skip-if-larger --strip --ext=.png --force "$f"

  # Rename to communicate the majority of file size savings complete
  # Allows me to grab the file faster
  new_f="$(dirname "$f")/$(date -r "$f" +"%Y-%m-%dT%H.%M.%S").png"
  mv -n "$f" "$new_f"

  # Improve deflate compression for an extra 10%
    /usr/local/bin/zopflipng -y "$new_f" "$new_f"

  # Copy to clipboard (optional)
  # osascript -e "set the clipboard to (read (POSIX file \"$(perl -e "print glob('$new_f')")\") as {«class PNGf»})"
done
```

Finally, I add the Screenshots folder to my Dock for easy access. This is achieved by dragging the Screenshots folder from Finder to your Dock.
