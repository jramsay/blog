---

title: 'Week 16, 2017'
slug: week-16
type: post

---

When I was studying I spent a lot of money on textbooks that I barely used. [Here is a textbook](https://minireference.com/blog/no-bs-linear-algebra-book/) trying to change that. A stats textbook is also coming later this year.

---

This week I learned that the 'Fearless Girl' statue installed in front of 'Charging Bull' on Wall Street was part of a marketing campaign for NASDAQ listed SHE. I wasn't aware of the history of 'Charging Bull' either. I'm less excited by 'Fearless Girl' knowing that the original guerilla art is being subverted by faux-guerilla art. On balance though, I do like it and hope it sticks around. [Read more here](https://gregfallis.com/2017/04/14/seriously-the-guy-has-a-point/).

---

This week [The Guardian ceased publisher articles on Facebook's Instant Articles and Apple News](http://digiday.com/media/guardian-pulls-facebooks-instant-articles-apple-news/). Particularly when Instant Articles launched there was a lot of bluster about saving journalism, but this was wishful thinking from the start. Publisher's absolutely need to provide great reading experiences that load quickly, but farming the content out to platforms you don't control won't magically create a loyal and profitable audience. [The Verge](http://www.theverge.com/2017/4/16/15314210/instant-articles-facebook-future-ads-video) has a good piece breaking it down.

Incidentally, I was linked to an interesting article on Variety from Twitter, which I tried to read on my phone earlier this week. The page was so clogged with interruptive scrolling units that I gave up. I had intended to try again on my laptop but can't remember what the article was. You can be sure I won't be clicking through to that property again any time soon. I can't tell if publishers don't care or just don't realize how awful the reading experience on their mobile sites is.

