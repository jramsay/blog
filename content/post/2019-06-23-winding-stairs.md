---

slug: winding-stairs
date: 2019-06-22T16:24:50Z
location: Amman, Jordan
photos:
  - /uploads/2019/9O3j_DrOv.jpg
  - /uploads/2019/4-QUSMWSF.jpg
  - /uploads/2019/RcgCOIXsf.jpg
categories:
  - Photos

---

Walking up and down the winding stairs of Amman.
