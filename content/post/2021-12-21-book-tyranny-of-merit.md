---

slug: 'book-tyranny-of-merit'
type: post
date: 2021-12-21T21:34:42+11:00
categories:
- Books

---

Finished reading: [The Tyranny of Merit: What's Become of the Common Good](https://www.amazon.com/dp/0374911010/) by Michael J. Sandel 📚

