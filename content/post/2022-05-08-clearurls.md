---

title: ClearURLs
slug: 'clearurls'
link: https://github.com/ClearURLs/Addon/
type: post
categories:
- Tech

---

[ClearURLs](https://github.com/ClearURLs/Addon/):

> Many websites use tracking elements in the URL (e.g. https://example.com?utm_source=newsletter1&utm_medium=email&utm_campaign=sale) to mark your online activity. All that tracking code is not necessary for a website to be displayed or work correctly and can therefore be removed—that is exactly what ClearURLs does.

Stops URL based tracking and improves the readability of URLs. Perfect. 👌
