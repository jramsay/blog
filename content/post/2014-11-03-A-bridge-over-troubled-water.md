---

title: 'A bridge over troubled water'
slug: a-bridge-over-troubled-water
type: post

---

<i>Originally published on Adslot company blog.</i>

Talk in the industry of late has been focused around programmatic pricing, transparency, and responsiveness. A recent report by The Economist[^1] suggests "60-80% of ad spending is siphoned off by ad-tech firms which take advantage of the market's opacity" supporting their claim that programmatic buying is failing to deliver its promised transparency and efficiency. This is not a new claim.[^2] Opaque fees and trading desk margins, often charged by agency owned DSPs, continue to be a source of concern to publishers.[^3] Transparent reporting of fees and markups will go some way to earn the trust of publishers and advertisers. Unaddressed however is the broader efficiency goals of automated sales channels, particularly automated guaranteed.

Meaningful automation of advertising transactions requires making good on the promise of campaign life cycle efficiency improvements. More specifically, the tools of the future must help produce better outcomes for advertisers, and improve agency and publisher efficiency throughout planning, trading, optimising, and billing.[^4]]

### Best laid plans

Jared Belsky (President, 360i) recently called for a 'Programmatic Reform' which identified an urgent need to overcome opaque operating models, integration gaps, and provide 'nimbleness and fluidity.'[^5] Responding to measured performance, external events and client feedback are cornerstones of effective campaign management, and necessitate a suite of integrated communication and planning tools.

Can automated guaranteed make good on the promise of efficiency if, during a campaign, planners and buyers are left scrambling for the phone, recording adjustments in disparate spreadsheets and leaving finance to pick up the pieces? Surely not. Efficiency should unite measurement and optimisation. Efficiency should bring order and confidence to billing processes. Efficiency should integrate communication and negotiation.

### Making good

Significant in roads have been made on creating automated guaranteed marketplaces which assist buyers discover relevant products, availability and pricing with ease. Yet, it would be a sad thing to reduce automated guaranteed to only this. Let’s talk about how timely reporting, real-time impression forecasting, and ad server integrations (both 1st party and 3rd party) could become the foundation of a toolset which helps agencies and publishers work together over the life of a campaign. Clients aren’t paying for a perfect media schedule, they are paying for results.

Until tools begin to help agencies and publishers collaborate post-purchase, and throughout the optimisation process, automated guaranteed won’t be living up to its greatest promise.

[^1]: [Buy, buy, baby: The rise of an electronic marketplace for online ads is reshaping the media business](http://www.economist.com/news/special-report/21615872-rise-electronic-marketplace-online-ads-reshaping-media-business-buy), The Economist (13 September 2014)
[^2]: [Publishers, Let’s Talk Programmatic CPMs](http://adexchanger.com/marketers-note/publishers-lets-talk-programmatic-cpms/), Ad Exchanger (28 May 2014)
[^3]: [The Rise Of Private Exchanges And The Future Of Agency Trading Desks](http://www.adotas.com/2014/04/the-rise-of-private-exchanges-and-the-future-of-agency-trading-desks/), Adotas (9 April 2014)
[^4]: [Who’s to blame for lagging Automated Guaranteed adoption? Everyone](http://adexchanger.com/data-driven-thinking/whos-to-blame-for-lagging-automated-guaranteed-adoption-everyone/), Ad Exchanger (19 September 2014)
[^5]: [The Time Has Come for Programmatic Reform](http://adage.com/article/agency-viewpoint/time-programmatic-reform/294833/), Ad Age (10 September 2014)
