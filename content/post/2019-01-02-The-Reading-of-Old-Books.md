---

slug: 'reading-old-books'
link: https://reasonabletheology.org/cs-lewis-on-reading-old-books/
type: post

---

C.S. Lewis writes in the introduction to [Athanasius: On the Incarnation](https://www.ccel.org/ccel/athanasius/incarnation).

> It is a good rule, after reading a new book, never to allow yourself another new one till you have read an old one in between. If that is too much for you, you should at least read one old one to every three new ones.

> Every age has its own outlook. It is specially good at seeing certain truths and specially liable to make certain mistakes. We all, therefore, need the books that will correct the characteristic mistakes of our own period. And that means the old books.

As the new year begins I have a long list of books to finish which I intend to intersperse with old books. I'll likely land closer to the one to three ratio, than one to one.
