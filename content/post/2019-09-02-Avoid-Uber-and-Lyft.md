---

slug: 'avoid-uber-lyft'
link: https://xoxofest.com/guide/transportation
type: post

---

[XOXO 2019](https://xoxofest.com/guide/transportation):

> We strongly encourage attendees use any of the options above instead of Uber or Lyft. Both companies are actively undermining public transportation and worker protections, refusing to pay their drivers a living wage or benefits, while artificially and unsustainably lowering the cost of rides.

I prefer to walk or cycle. Failing that, I use public transportation for longer distances. Too many cities make this difficult, or dangerous, if not impossible.
