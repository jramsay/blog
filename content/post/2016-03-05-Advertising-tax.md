---

title: 'Advertising is becoming a tax that only poor people pay'
link: 'https://www.youtube.com/watch?v=jfjg0kGQFBY'
people: ['Scott Galloway']
slug: advertising-tax
type: post

---

Scott Galloway, Professor of Marketing and Brand Strategy at NYU Stern and Founder of L2 said at DLD:

> The advertising industrial complex is about to come to an end and the downstream service providers–the conglomerates–are about to take their turn at the woodshed.

> The house that advertising built was consumer packaged goods. They taught us that detergents and soaps could be wrapped in emotion. You were a better mum, you were more American, you were a more elegant European if you used a certain type of hand soap. This is the house that advertising built.

> Last year the house that advertising built, almost 90% of all CPG brands lost share, and two-thirds lost revenue. Why? Because advertising sucks! And if you're wealthy you can opt-out of advertising. _We are now downloading Modern Family and paying two-bucks [$2] for it on iTunes solely so we can avoid the advertising._ Advertising is becoming a tax only poor people pay.

Scott follows the money and describes my individual experience precisely.
I pay for content on iTunes and Netflix, and subscribe to the few publications I read regularly to avoid advertising.

Most advertising is irrelevant and low quality and I'd rather not give up yet more of my privacy in the hope of seeing more relevant advertising.
Those with the means are going to opt-out by paying for content or install an ad blocker to bypass advertising all together.
