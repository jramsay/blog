---

title: 'The Most Powerful Lesson I’ve Ever Learned In Business'
link: 'https://medium.com/@mmccue/the-most-powerful-lesson-i-ve-ever-learned-in-business-4d89e95ab250'
slug: powerful-lesson-in-business
type: post

---

Mike McCue writes on Medium:

> If you seek out and uphold the first principles you will not only make a good decision, you’ll do it in way which strengthens your team rather than splintering it.

Mike writes about using first principles when making tough decisions and provides a practical example from his own experiences.

> After all, integrity is all about upholding principles… And _principles only matter when they’re hard to keep._

Good advice, but hard to follow when it counts.
