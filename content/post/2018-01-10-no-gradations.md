---

slug: 'imago-dei-no-gradations'
link: http://kingencyclopedia.stanford.edu/encyclopedia/multimediaentry/doc_the_american_dream/index.html
type: post

---


[Martin Luther King, Jr.](http://kingencyclopedia.stanford.edu/encyclopedia/multimediaentry/doc_the_american_dream/index.html) at Ebenezer Baptist Church, Atlanta, Georgia, on 4 July 1965:

> The whole concept of the imago dei, as it is expressed in Latin, the "image of God," is the idea that all men have something within them that God injected. Not that they have substantial unity with God, but that every man has a capacity to have fellowship with God. And this gives him a uniqueness, it gives him worth, it gives him dignity. And we must never forget this as a nation: _there are no gradations in the image of God._ Every man from a treble white to a bass black is significant on God's keyboard, precisely because every man is made in the image of God. One day we will learn that. _We will know one day that God made us to live together as brothers and to respect the dignity and worth of every man._

A timely reminder.
