---

title: Backup to Synology NAS using Arq and MinIO, without Docker
slug: synology-backup-guide
date: 2020-04-08T10:19:26Z
categories:
- Tech

---

**Update:** No need to compile MinIO yourself anymore — MinIO is available on [SynoCommunity](https://synocommunity.com/package/minio).

I use [Arq](https://www.arqbackup.com/) to backup my computer to [Dropbox](http://dropbox.com/) (off-site backup) and a [Synology NAS](http://synology.com/) (on-site backup). The most obvious approach is to use a folder stored on your NAS. I previously explained how using [Autofs](/2017/11/29/macos-autofs/) can make this easier, but I've still found it unreliable and slow.

Arq calculates checksums when performing a backup to verify uploads, and when validating backup data every 60 days. When using the folder approach, Arq needs to download the data to calculate a checksum. S3-compatible storage, however, can calculate the checksum, allowing this work to be offloaded to the NAS making backups and validation faster.

Arq supports S3-compatible storage including [MinIO](https://www.minio.io/), which can be run on a Synology NAS using [Docker](https://www.synology.com/en-us/dsm/packages/Docker). Unfortunately, only Synology devices with an x64 CPU can run Docker. My DS216play has an ARMv7 CPU, so I can't use the Docker package.

Here's how you can compile MinIO to run natively on an ARMv7 Synology NAS without Docker. 🎉

### Compile MinIO

Before you begin, you need [Git](http://git-scm.com/) and [Docker](https://www.docker.com) to be installed.

Clone the [MinioSPK](https://github.com/darkmuggle/MinioSPK) repository, which contains build scripts for creating a Synology MinIO package.

```bash
# Clone the project
git clone https://github.com/darkmuggle/MinioSPK.git

# Change directory
cd MinioSPK
```

Two commands are needed to compile MinIO.

```bash
# Download the MinIO source and logos
make init

# Build the MinIO package for Synology
make arm7
```

### Install MinIO on our Synology NAS

With our newly built package, we are ready to install MinIO.

From **Synology DiskStation**, the web interface for managing your Synology NAS, open **Package Center**.

Next, from the **Installed** section (top left), select **Manual Install** (top right) to launch the installation and upgrade wizard.

Click **Browse** to locate the package you compiled, likely similar to `minio-2020.04.04-arm-7.spk`. Then click **Next**.

After a short while you'll be shown a warning because package hasn't been signed. You will need to click **Yes** to proceed.

The wizard will prompt you to set the `MINIO_ACCESS_KEY` and `MINIO_SECRET_KEY`, and then install MinIO. Once installed you will be able to open MinIO from Package Center. Use the access key and secret key you set during the installation to login.

### Configure Arq

Now that you've installed MinIO, you can use the guide on the Arq blog [Backup to Synology NAS using Minio and Arq](https://www.arqbackup.com/blog/synology-backup-guide/).
