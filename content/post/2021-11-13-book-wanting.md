---

slug: 'book-wanting'
type: post
categories:
- Books

---

Finished reading: [Wanting: The Power of Mimetic Desire in Everyday Life](https://www.amazon.com/dp/1250262488) by Luke Burgis 📚

