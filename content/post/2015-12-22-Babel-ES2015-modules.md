---

title: 'Babel ES2015 transpiled module compatibility with CommonJS'
link: 'http://www.2ality.com/2015/12/babel-commonjs.html'
people: ['Axel Rauschmayer']
slug: babel-es2015-modules
type: post

---

If you're using Babel, Axel Rauschmayer's recent post [Babel and CommonJS modules](http://www.2ality.com/2015/12/babel-commonjs.html) is worthwhile reading to understand how interoperability between transpiled and CommonJS modules is achieved.
