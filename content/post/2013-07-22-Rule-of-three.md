---

title: 'Rule of three'
link: 'http://www.codinghorror.com/blog/2013/07/rule-of-three.html'
people: ['Jeff Atwood']
slug: rule-of-three
type: post

---

A perl from [Jeff Atwood](http://www.codinghorror.com/blog/2013/07/rule-of-three.html):

> To build something truly reusable, you must convince three different audiences to use it thoroughly first.

A clever and concise rule of thumb for software products.
