---

title: 'Week 32, 2017'
slug: week 32
type: post

---

Reading The Atlantic this week reminded me it is time I subscribed. If you love great journalism you should pay for it! Online advertising doesn't work. The New York Times [digital ad revenue in 2016](https://www.nytimes.com/2017/02/02/business/media/new-york-times-q4-earnings.html) was $209M, while their monthly unique users [grew to 92M in January 2017](http://247wallst.com/media/2017/02/25/new-york-times-moves-ahead-of-washington-post-in-digital-audience-race/), which nets out to approximately $2.27 per unique user in 2016. In a year, you almost undoubtedly paid more to download the ads than they received from the advertiser for you viewing the ad. If you can spare half an hour, Scott Galloway's [Death of the Industrial Advertising Complex](https://www.youtube.com/watch?v=yOpSpQAxCHU) is entertaining and worthwhile.

---

I used my phone less while in Tasmania last week and it was a great feeling. Every time I reduce my usage I feel better for it. This week The Atlantic asks [Have Smartphones Destroyed a Generation?](https://www.theatlantic.com/magazine/archive/2017/09/has-the-smartphone-destroyed-a-generation/534198/). A tough headline to live up to, yet it came closer than I expected to making the case. Even if a generation hasn't been destroyed, I do think society has a problem on it's hands learning to manage the addiction of our digital devices and software which reduce the frequency and quality of our face to face interactions.
