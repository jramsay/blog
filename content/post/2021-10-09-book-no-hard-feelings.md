---

slug: 'book-no-hard-feelings'
type: post
categories:
- Books

---

Finished reading: [No Hard Feelings: The Secret Power of Embracing Emotions at Work](https://www.amazon.com/dp/0525533834/) by Liz Fosslien and Mollie West Duffy 📚

