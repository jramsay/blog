---

title: SoundSource
slug: 'soundsource'
link: https://rogueamoeba.com/soundsource/
type: post
categories:
- Tech

---

[SoundSource](https://rogueamoeba.com/soundsource/) is "sound control so good, it ought to be built in." It is a menu bar app that makes it easy to switch input and output devices, adjust levels, and add effects like an equalizer.

Regular video calls and recordings mean I want to hear and be heard - SoundSource makes that easier for me. Two features I particularly appreciate:

- [Super Volume Keys](https://rogueamoeba.com/support/knowledgebase/?showArticle=SS-SVK) makes the volume keys work when you connect your headphones through an external display or dock.
- [Custom Profiles for Headphone EQ](https://rogueamoeba.com/support/knowledgebase/?showArticle=SoundSource-Custom-HPEQ) can be used to make loud outputs quieter so that the volume keys are useful.

This is the custom profile I created for my CalDigit dock because the audio output was too loud at even the lowest setting.

```
Preamp: -15.0 dB
Filter 1: On PK Fc 0 Hz Gain 0 dB Q 1
```

It would make my day if SoundSource would remember the last used Headphone EQ for each output device so that I don't switch profile manually.
