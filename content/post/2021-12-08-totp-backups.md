---

title: Authy and Symantec VIP Access TOTP
slug: 'authy-symantec-totp'
type: post
date: 2021-12-08T11:26:43+11:00
categories:
- Tech

---

There seems to be an increasing number of services that require a specific app for two-factor authentication. In the last week I needed to install both [Authy](https://www.authy.com/) and [Symantec VIP Access](https://vip.symantec.com).

I'd prefer, however, to keep all my Time-based One-Time Passwords (TOTP) in one place. Having secret keys scattered over multiple apps makes them hard to backup. Thankfully I found solutions for both:

- Authy secret keys can be extracted from the desktop app using following these [instructions](https://gist.github.com/gboudreau/94bb0c11a6209c82418d01a59d958c93).
- Symantec VIP Access can be avoided by generating a secret key with the [python-vipaccess](https://github.com/dlenski/python-vipaccess) tool. I found I needed to set the token model to `SYMC` in most cases (`vipaccess provision -p --token-model SYMC`).

