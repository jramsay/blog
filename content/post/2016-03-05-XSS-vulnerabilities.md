---

title: 'Widespread XSS Vulnerabilities in Ad Network Code Affecting Top Tier Publishers, Retailers'
link: 'http://randywestergren.com/widespread-xss-vulnerabilities-ad-network-code-affecting-top-tier-publishers-retailers/'
people: ['Randy Westergren']
slug: xss-vulnerabilities
type: post

---

Randy Westergren writes:

> Any time we allow 3rd party scripts to run on our sites, we effectively relinquish control of the code that executes on the client. This is particularly important when integrating ad network scripts since they are inherently more dynamic than most other types of integrations, the cause of which is the ad industry’s general fragmented nature.

Randy details the vulnerability and lists high profile vulnerable sites including [The Telegraph](http://telegraph.co.uk), [NYPost](http://nypost.com), [CBS News](http://cbsnews.com), [NBC News](http://nbcnews.com), [NYTimes](http://newyorktimes.com), [MSN](http://msn.com), [Washington Post](http://washingtonpost.com) and the [BBC](http://bbc.com).

It's always surprised me that publishers are so willing for third-party scripts to be served to their customers over the RTB exchanges without any real quality control tools.
Even guaranteed advertising presents quality control challenges for publishers who are limited to sampling the responses of agency ad tags, rather than being able to examine the underlying creative or tracking scripts directly or be notified of changes being made by the buyer.

Given vast coverage of advertising across the web and their vulnerability to XSS attacks, it is surprising so few have attacks have been documented.
Yet another reason to run an ad blocker, despite the financial hurt they cause to publishers.
