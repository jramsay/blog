---

title: People's Deepest, Darkest Google Searches Are Being Used Against Them
link: 'http://www.theatlantic.com/technology/archive/2015/11/google-searches-privacy-danger/413614/'
slug: google-search-curates-financially-vulnerable
type: post

---

Business that knowingly take advantage of the financially vulnerable make me sick. Increasing the access that pay-day lenders have to people in these situations through lead generators is sad and terrifying, but as the article [People’s Deepest, Darkest Google Searches Are Being Used Against Them](http://www.theatlantic.com/technology/archive/2015/11/google-searches-privacy-danger/413614/) points out, Google is in a tough situation trying prevent this and uphold it’s own policies, let alone any hypothetical increased regulation. A terrible problem without an obvious solution.
