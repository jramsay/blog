---

title: 'RTB Is Anti-Publisher At Its Core'
link: 'http://www.mediapost.com/publications/article/269598/rtb-is-anti-publisher-at-its-core.html'
people: ['Todd Garland']
slug: rtb-is-anti-publisher
type: post

---

Todd Garland of [Buy Sell Ads](http://buysellads.com/) writes for MediaPost:

> When did it become acceptable for advertisers to allocate a perceived value to a publisher’s inventory? If we’re looking for examples to help us define the concept of onerous terms, look no further than the ecosystem built, and continually propped up by, RTB advocates.

> _I’d like to say that it's insane._

> Imagine walking into a car dealership and then simply telling the salesperson what you will be purchasing a car for. Do you think a dealership would let you walk out with the keys? That’s exactly how RTB exchanges work today.

Only a few days prior, Todd wrote on [LinkedIn](https://www.linkedin.com/pulse/its-time-ad-tech-evolve-todd-garland):

> The IAB and the house it built is a mess, and it’s dangerously close to catching fire and burning to the ground. The IAB still continues to ignore the simple fact that people have voted with their Chrome, Firefox, and Safari plugins. _The market reality we’re all facing is something the ad tech industry has created and end-users don’t give two “merdes” about what that means for publishers._

Todd doesn't pull any punches and says publicly what many have been thinking.
More frequent critical appraisals like these would benefit the advertising industry.
