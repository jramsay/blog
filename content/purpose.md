---

title: Purpose
noTitle: true

---

`/Purpose`

This site exists principally for me to collect fragments of thoughts, glimmers of ideas and occasional observations, but I do hope that you will also benefit from the words collected here.

Secondarily, _it is an experiment in building a lean and functional blog._
This interested me because I, like many others, have observed the size of most sites increasing rapidly.

My objectives for this site were:

- Fastest possible load time on empty cache
- Zero external assets or scripts for privacy
- Simple text only design

Through embedding the tiny CSS and carefully trimming unnecessary characters from the webfont, I've been able to keep the page size and first load very fast.
The biggest performance improvement I could make would be a redesign using a native font, but I like Merriweather and that would be one less element to tune.

If you have ideas to make the site faster, let me know on [Twitter](http://twitter.com/jamesramsay) or open a pull request on Github. I'd love to hear your thoughts.

↬ [/purpose](http://blog.fictivekin.com/post/51570286730/slash-purpose)
