---

private: true
title: "[Redeemer] Romans: In Christ Jesus: How the Spirit Transforms Us"
date: 2006-11-19
link: https://gospelinlife.com/downloads/in-christ-jesus-how-the-spirit-transforms-us/
image: https://gospelinlife.com/wp-content/uploads/2017/09/series_in_christ_jesus_how_the_spirit_transforms_us_1-1-400x400.jpg
episodes: "dropbox://rpc_romans_transformation/episodes.yml"
outputs:
- podcast

---

{{< podcast_links >}}

How does faith in Jesus actually change a person? What is the process? In Romans 6-8, the Apostle Paul, more fully than in any other place in the Bible, reveals how faith in Christ leads concretely to growth in character. We all wrestle with the thought that we are not the people we want to be. Paul neither denies the truth of our brokenness, nor holds a pessimistic view of our prospects. He grounds the hope of our new identity in the work of Christ and the Holy Spirit, for us and in us. If God is for us, who can be against us?
