---

private: true
title: "[Redeemer] 1 Peter: Splendor in the Furnace (Part 1)"
date: 1993-09-13
link: https://gospelinlife.com/downloads/splendor-in-the-furnace-1-peter-part-1/
image: https://gospelinlife.com/wp-content/uploads/2017/09/series_splendor_in_the_furnance_1_1-1-400x400.jpg
episodes: "dropbox://rpc_1peter_part1/episodes.yml"
outputs:
- podcast

---

{{< podcast_links >}}

It is not a trifling thing to wonder if there is God who is all good and all powerful, then how can suffering exist? The Bible does not give us an easy, pat answer to this. However, it has a lot to say about how God relates to us in our suffering, going so far as to assert what no other religion does, that God has actually suffered on our behalf. Hard times come. We will all inevitably suffer. The apostle Peter shows us that even as we go into the furnace of our trials, God provides us with resources that turn the heat into a refiner’s fire. We can emerge from the flames as pure gold.
