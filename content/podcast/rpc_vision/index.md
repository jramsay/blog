---

private: true
title: "[Redeemer] The Vision of Redeemer"
date: 2005-09-11
link: https://gospelinlife.com/downloads/walking-in-the-name-of-the-lord-our-god/
image: https://gospelinlife.com/wp-content/uploads/2017/09/series_the_vision_of_redeemer_13-1-400x400.jpg
episodes: "dropbox://rpc_vision/episodes.yml"
outputs:
- podcast

---

{{< podcast_links >}}
