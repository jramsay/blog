---

private: true
title: "[ABC Classic] For The God Who Sings"
date: 2022-01-01
link: https://www.abc.net.au/classic/programs/for-the-god-who-sings/
image: https://www.abc.net.au/cm/rimage/13895996-1x1-large.jpg?v=2
episodes: "dropbox://abc_ftgws/episodes.yml"
outputs:
- podcast

---

{{< podcast_links >}}

Sunday, 10pm to midnight.

Celebrating music for the spirit from throughout the centuries.
