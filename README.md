# www

Template forked from [Marfa](https://github.com/microdotblog/theme-marfa).
Generated using [Hugo](http://gohugo.io).
Served by [Zeit](https://zeit.co/).

Items of interest:

- json, text formats

Roadmap:

- Indie Auth
- Micropub endpoint
