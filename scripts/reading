#! /usr/bin/env bash

# Generate post stubs for articles I read last week

# Output: 2018-10-02-foo-bar.md
# ---
#
# title: Foo bar
# slug: foo-bar
# link: https://example.com
#
# ---
template="---\n\ntitle: %s\nslug: %s\nlink: %s\n\n---\n"

to_slug() {
  # Forcing the POSIX local so alnum is only 0-9A-Za-z
  export LANG=POSIX
  export LC_ALL=POSIX
  # Keep only alphanumeric value
  sed -e 's/[^[:alnum:]]/-/g' |
  # Keep only one dash if there is multiple one consecutively
  tr -s '-'                   |
  # Lowercase everything
  tr A-Z a-z                  |
  # Remove last dash if there is nothing after
  sed -e 's/-$//'
}

branch="reading/$(date +%F)"

git checkout -b "$branch"

items=$(curl -L "http://feed2json.org/convert?url=https%3A%2F%2Fwww.reading.am%2Fjames%2Fposts.rss" -s \
  | jq --arg LASTWEEK $(date --date='7 days ago' +"%FT%T") \
      '.items[] | select(.date_published > $LASTWEEK and (.summary | startswith("✓")))' -c)

counter=0

while IFS= read -r item
do
  title=$(printf '%s\n' "$item" | jq -r '.title | capture("James Ramsay is reading \"(?<title>.*)\"") | .title')
  url=$(printf '%s\n' "$item" | jq -r '.url')
  slug=$(printf '%s\n' "$title" | to_slug)

  printf -- "$template" "$title" "$slug" "$url" > content_blog/posts/$(date +%F)-$slug.md

  git add src/posts/\*.md
  git commit -m "✌️ $title"A

  counter=$((counter+1))

done < <(printf '%s\n' "$items")

if [[ "$counter" -gt 0 ]]; then
  git push -u git@gitlab.com:jramsay/blog "$branch"
fi
