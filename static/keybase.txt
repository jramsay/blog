==================================================================
https://keybase.io/jramsay
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://blog.jramsay.com.au
  * I am jramsay (https://keybase.io/jramsay) on keybase.
  * I have a public key with fingerprint 5494 061B 114C 7468 EF2B  6179 1BCD 63BD 7FAF 4FBC

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "010135b022c3df020c3540a2bb359e62af151e826c25836c68ed42c5c66577a89d7b0a",
      "fingerprint": "5494061b114c7468ef2b61791bcd63bd7faf4fbc",
      "host": "keybase.io",
      "key_id": "1bcd63bd7faf4fbc",
      "kid": "01016752704bb55b2aa4d93fd3e67e7e9961c9d5b10f521d1e8b1857fe610b05d4c00a",
      "uid": "d55cc0863930be6f79ea096383a96f19",
      "username": "jramsay"
    },
    "service": {
      "hostname": "blog.jramsay.com.au",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1505771699,
  "expire_in": 157680000,
  "prev": "57daf4635c29ae39f2e34418a493aeef5a05fbdc2d5a042a64ef36ba68f4b8fe",
  "seqno": 24,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----

owFtUntMU1ccLggGQQmiMTgqjysYHKXe96PuwRBIjMxgWEWYs7nn3nPbW6BlbXkF
WKkLmajBTXmNgWaionNhk+kw4guGyMYEB64BzSaDiWAmSLY6t4xttwSzf3b+Oef8
8n3f+X7f+b2/YokqwOf2D18mjU3qz/v0TwNV1vXEa6UIsIoliK4UyYELG8wVod1h
yJFFRIegGIoRFEBxXCBECcVRgaBIlMcBICgO0jgvYRQGWZwWcIolaIFmoUjiAiXQ
NMUwPMuJDEB5RINIssUIbfk22eJQZCmSI1EaAxhGCgypkCQc0BjDYUAQaQKIjMRL
pAQEhWiy2r0MxRzg7VArW5WacjEs2Psf/H++aYbCGZQEgKIAzvOkyBGSSECagQzk
OBoTOJECGCpROCYqPQCMpRgJ0hgKUEokBXTBd8GCnEhRgoCyNMERKIC0xHCQRzma
YAmeoyWM8wLt0Gbh86CCNtv4PDtfgpRrEKVYKAvQm6u3kUUAyLUatYsorWDN0/IF
ikK+zeqwCtZcBWByOPLtOq+AoyTfyyiCwLCoZQCyRVTSVBiF0GaXrRZEhylIwSF7
xTEKVZLHaI7TILA4X7ZBg+xFUAzNosryvgMLvX9gYEQlNZqgBJzjIcFJOCRIEmN5
kiN4CCWKRykJiAIuKicS52kSSgQNeJqVSMBKEPG297bFiuhwUjHKGxVRu2y08I4C
G0TKA6t8CT+VT4Bq3Zpov6JS/sKviWFL9XOJh56PoL+vd/5UgctCnle4jSGqzpni
iPeSTJX9yb5HWja7Tp8Y1qRNC/5Pu2++Q2ZtfzTHvqUOKP3mz/ArV/vaPY05J/aP
D/hmxK2O37xudll9JzpzLHXptrNdkZUFfsxLUU9zzoKw4B0Vw7EHXQNb6hrTH5kD
OtKq13frN+BhV/bPKQOMu/44fW5L0BfZR3WfHygbf82SOP9iaGv9tU1FY12jR9wV
O+f7Zr8OffP+5b7CS3UNK3f0p1VJPepV7iDOad90AK8MvdGyFUmtGGwSXjCefGza
u+FwclbtZ3XnMrTbjkd2euajM/BmcMn8vStmVFvzuHpLasRYz7fHa4ZdHZkJa8Z/
jBvq5xp2zcmr98TKCb3ltV3mUf8y99+e+64u4vfDnXOta+Nu+4+0xZ9aNSL+vNWz
u2bFDOdsS+sdqHoirZ2caI875n73TqIu/tmt5SvDW3ftNWlSRX10c/HDq9qD3dMX
1T99ei894nz9y4Q7fWpi9C+jp93U/NC8PlUf2HjhqxF15mDUq69M5jrcrjsJ5Xpn
0s68sFN7Ovxe/1hN/qOnBns9QXfNy1s6hTcyz3xwN5hwfnj5t4mPfrkFLVMxUWLZ
jeDrnux5Z0p3cUz1kqLq73qc+4TgOrq1tbTBnXF04ydCc1RH0z6r5tnuk8kPHsTH
UkPq7V0Jbe1nZk1NKe6p2sibF8PBRHbKocjAkCFHKP7EcW/4Xw==
=1IzB
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/jramsay

==================================================================
